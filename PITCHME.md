
## Managing sensitive data

### Aidan Wilson
Research Consultant, eResearch Analyst
aidan.wilson@acu.edu.au

---

## Outline

- What is sensitive data? |
- Where can I store it? |
- Can I ever share it? |
- How can I protect privacy? |
- How do I collect it? |

Note:
- I'll first introduce what I mean by sensitive data
- I'll give you some practical advice on where you should be storing all data, including sensitive data
- I'll discuss when and how you can share it with collaborators,
- I'll give you a very simplistic demonstration of how to protect against a privacy breach,
- And I'll discuss a couple of options for collecting survey data that place an emphasis on privacy.
Before I start, a disclaimer, this talk does not constitute legal advice.

---

![cartoon](https://teachprivacy.com/wp-content/uploads/Cartoon-HIPAA-Medical-Record-Online-01.jpg)

Note:
While completing this presentation, I realised this was a terribly dry topic, so I went on the hunt for some imagery to raise the levity a bit. This is the best I could find.

Ok, the levity ends there.

---

## What is sensitive data?

Sensitive data are data that can be used to identify an individual, species, object, or location that introduces a risk of discrimination, harm, or unwanted attention. Major, familiar categories of sensitive data are:

- personal data
- health and medical data
- ecological data that may place vulnerable species at risk

Source: [ANDS](https://www.ands.org.au/working-with-data/sensitive-data/sharing-sensitive-data)

Note:
So if you're collecting health and medical data, it is by this definition, sensitive, and so certain controls apply, which I'll talk more about throughout this talk.

Before I go on, notice the exact verb phrase here: "can be used to identify". 

+++

### Deidentified ≠ Unidentifiable

![identifiable](assets/identifiable.png)

@ul

- Deidentified? *Arguably*
- Reidentifiable? *Definitely!*

@ulend

Note:
Here is an example of a very small dataset to demonstrate what is meant by reidentifiable. Bear in mind this data, and all data I'm using in this presentation, is fake.

The contact information has been removed so it's in a sense 'deidentified', but the data itself is so personal, that it wouldn't take a genius to make a good guess as to the individuals these records refer to. This postcode is a small town literally out back of Bourke called Louth, population 43. Individuals can easily be identified on the basis of these fields.

This is important for talking about sensitive data. It's not sufficient to say that since you're not collecting contact information, your data is not sensitive. This kind of data is often referred to as Personal identifiable information, or PII.

If you've ever explored the Australian census data, you might occasionally come across a means to protect privacy without realising it.

+++

![louth_census](assets/louth_census.png)

Note:
Here is the breakdown of the indigenous status from the 2016 census, looking specifically at Louth. With a total population in 2016 of 38, it makes for a very good example of this.

Notice that the number of people who identify as Aboriginal in Louth is 3. It is probably likely either 2 or 1, but the Census won't let you go lower than 3 when filtering, because if I lived in Louth, I could reasonably identify that individual, and cross reference their responses to other questions.

So when allowing others, the public, say, to look at your data, consider the identifiability of the information you present, and if necessary, look into methods to obscure individual data points. Putting in thresholds, as in this example, is one method. Only publishing aggregate data, is another.

---

## Where can I store it?

Do *not* use:

- A removable hard drive or thumbdrive |
- Dropbox |
- OneDrive |
- SharePoint |

Note:
Don't store sensitive data on removable media, as it's susceptible to loss, theft, or data corruption.

None of these services is designed to store research data, and none are appropriate to store any kind of sensitive data. The ARC rules state that sensitive research data may not leave Australia without a good reason, and the only acceptable reasons are, for example, that you're working with a collaborator in Europe and so it is unavoidable that data should also reside there.

+++

## Where can I store it?

Preferred options:

- Shared Drive/Personal drive (Isilon) |
- CloudStor |
- Intersect Space |

Note:
Shared drive? Yes. These are locally managed data storage locations and the data resides in ACU's data centres. Access to data requires an ACU account and requires the user to be within the ACU network; either on-site physically, or connected to the VPN. It is therefore very secure.

Intersect is a not-for-profit organisation that works closely with Australian universities to provide eResearch support. Disclaimer: I work for Intersect and am provided full-time to ACU to fill the need here. Intersect operates a fairly large data storage service called Space, which is very secure, supports collaboration, and is very large, but it is not free, so this option would only be for you if you had a lot of data to manage.

CloudStor? Yes. CloudStor is managed by AARNet, the Australian research network (essentially the ISP of all AUstralian universities).

CloudStor provides 100GB of cloud storage, accessible through a number of methods. Most commonly via a web interface, or using a local sync client. It essentially works like dropbox, except that it is suitable for storing research data. 

Access to CloudStor is mediated through the Australian Access Federation, meaning that you authenticate using your credentials from your home institution, your ACU account. This also allows for data to be shared with collaborators outside of ACU. There is even the possibility of sharing data with collaborators outside Australia, or outside research entirely.

Contact me if you need to share data with a collaborator and aren't sure how, or for any help with setting up CloudStor for storing sensitive data.

+++

## Post-project storage

- Keep research data for five years
  - or more depending on the specifics of your project |
- Archive data with Research Bank |
- Curate your data: |
  - What is sensitive |
  - What is openly accessible |

Note:
The Australian Code for the Responsible Conduct of Research, and ACU's Research Data Management Policy, above all, require you to keep raw data for a minimum of 5 years, and more in specific circumstances, like clinical trials, for which data needs to be retained for 25 years.

Research Bank is the ideal place to do that.

It is important when doing so to curate your data and to identify that information which is sensitive and should be restricted. Depending on a number of factors, your source of funding most importantly, you may be required to publish your results openly. While these two may seem in conflict, it might simply mean you openly publish a sufficiently deidentified dataset, such as aggregate level information.

Publishers should not expect you to provide access to any raw results or personal information.

For more information on archiving your research data with Research Bank, speak to me or the Library.

---

## Can I ever share it?

- Yes, provided you have consent, and take care |
- Share only anonymous results |
- Preferably share only aggregated results |
- Share using a secure platform like CloudStor |
- Never send sensitive data over email |

Note:
I hear a lot of the time that people are wary of sharing sensitive data, and a lot of researchers seem to think that their data is so sensitive that it can never be shared. I remain unconvinced that there's ever a dataset that cannot be shared without some basic preparation.

The answer to this question is yes, provided you have the expressed consent of your participants to do so, and as long as you maintain satisfactory control over who can access it, and exactly what they can access.

You should only share the research data and not identifying or identifiable information, and when sharing data, it is best to only share aggregated results, such as tables of figures showing averages, maxima, minima, standard deviations, etc., rather than individual participant data.

You should only use a secure mechanism to share data, like CloudStor. You should never use email to send data to anyone, including your supervisor.

A question here emerges, how can you only share anonymous results and not share identifying information?

---

![privacy](http://amberhawk.typepad.com/.a/6a0115709c6f9d970b014e8a2a7e90970d-800wi)

Note:
This is a useful cartoon at this point, because this isn't really about protecting against hacking, which you should obviously try to do, as much as you can, but against unintended breaches by not taking basic protective measures. Your participants have entrusted you with their data and, as I will explain in a few minutes, you have provided assurances that you will treat it with care.

So how can we do that?

---

## How can I protect privacy?

- Separate identifying information from data |
- Keep identifying information very safe |
- Use password protection at a minimum |
- Use encryption, or a service that automatically uses encryption |

Note: 
This is a vastly simplified example and you may never have to do anything like this, but it helps to illustrate it with such a basic example, as a lot of protective measures that take place automatically in systems like the two survey applications I'll be mentioning soon, are built on this fundamental approach.

The principle is, pretty simply, to separate the contact information from the research results. The research results, be they medical records, survey responses or interview responses, can be shared with collaborators (while still being careful of any information that could be used to identify someone) using a platform like CloudStor (do not ever email research data to collaborators or supervisors), and the contact details for participants you can keep in a separate location, accessible only to you. Someone would need to access both sources of information in order to compromise the privacy of your subjects.

For added security, you should keep the contact details in a password-protected archive. Zip the file up and enter a password. This will encrypt the contents and render them useless without the password. Obviously, do not store the password in the same location.

You should also make sure the service you are using for data collection is secure and uses encryption. Look for an https in the address bar or something like a survey application. This s stands for secure, and ensures the traffic between the user and the server is encrypted in transit. The same goes for services like CloudStor, which encrypts all data in transfer and at rest, meaning if a hacker got access to the raw data, it would be incomprehensible to them without the decryption key.

+++

### How can I protect privacy?

![deidentifying1](assets/deidentifying1.png)

Note:
Let's say you have a dataset like this (this is all fake data which I generated, in case you're worried), you'll see name, email, phone number and date of birth, alongside very sensitive, personal information like blood type, height, weight, BMI, and 5 blood glucose level measurements. This is a problem. If you want to send a collaborator the results so they can run some statistical analysis, you run the risk of sending them the contact details as well, which may put you in breach of privacy laws.

+++

### How can I protect privacy?

![deidentifying2](assets/deidentifying2.png)

Note:
The solution is to split this single repository of information into two separate databases. The first is a list of your participants and their contact details, without any of the research results.

+++

### How can I protect privacy?

![deidentifying3](assets/deidentifying3.png)

Note:
The results are stored in a separate file entirely, which can be more safely shared with collaborators so they can analyse your response data without knowing the identity of any of the individuals.

+++


### How can I protect privacy?

![deidentifying4](assets/deidentifying4.png)

Note:
Most importantly, when you split your results from your contact information, add a unique identifier, most simply, a number, that can be used to identify your participants in future if you need to.


+++

### How can I protect privacy?

- Password protect the identifying information |
- Keep both files separate |

Note:
Once separated, these two files need to be removed from one another. Zip up the identifying information into an archive and password protect it. Then, put it onto a separate storage platform, or at least a separate location on the one storage platform. 

Note that if either one of these two files was accidentally released, the impact would be fairly minor. At worst, some names and email addresses are released. It isn't great, but it's not the end of the world. However, if both of them were released, then you've suddenly got a very serious data breach, which the university would be required by law to report to the government.

Luckily, you probably won't have to do anything like this, because there are applications for running surveys and collecting form data that handle anonymity for you.

+++

![password-strength](https://imgs.xkcd.com/comics/password_strength.png)

Note:
Before I go on I might take a quick sojourn into the world of passwords.

As this comic illustrates, the passwords that most systems require you to have (at least one lowercase character, at least one capital, one number, one special character, no dictionary words, etc.) are very easy for computers to guess, and very difficult for humans to remember.

Even worse, this might encourage someone to reuse the same password they think is really great, across multiple services. I do this.

This is really dangerous. Not because your password is likely to be cracked, but because it's possible that one of those services will be hacked, and a list of credentials, username and password combinations, sold on the black market or simply dumped on the public web. Then, it's a short step to someone trying your Evernote or Adobe password on every other service they can think of, banks, ebay, paypal, email addresses. Once they get a login, they'll use that to traverse your accounts, and may end up being able to get into your ACU account.

+++

### Passwords

What can you do about this?

- Use a different password for every single service |
- Make each password very long (at least 20 characters) |
- Have them be completely random gibberish, not even based on dictionary words |

Note:
Well, what you should do is maintain a different, very good password for each different service that you have, reusing none of them. They should all be very long, be made of completely random gibberish, and not at all based on dictionary words.

And, by the way, passwords made up of patterns of traversing the keyboard, like 1qazxsw2, which is down the first column and up the second, are very predictable.

+++

### Passwords

What can you do about this?

- Use a password manager. |

Note:
Or, you can not bother with any of that and simply use a password manager, like onepass, lastpass, dashlane, Keepass, and so on. There are dozens of these with varying features and varying levels of free. I'll put up a link later to a list of good ones. In essence, they maintain the database of your passwords and keep them secure using a single, master password, that you simply must remember in order to unlock any of your passwords.
Usually these password managers will have browser extensions that will automatically detect that you have a password for the site your visiting, and offer to fill it in for you. They can also generate passwords for new sites when you are prompted to enter one.
Password managers really are very good, very user friendly and very secure. 

+++

### Passwords

![1password](https://9to5mac.com/wp-content/uploads/sites/6/2018/06/1password.jpg)

Note:
Or, you can not bother with any of that and simply use a password manager, like onepass, lastpass, dashlane, Keepass, and so on. There are dozens of these with varying features and varying levels of free. I'll put up a link later to a list of good ones. In essence, they maintain the database of your passwords and keep them secure using a single, master password, that you simply must remember in order to unlock any of your passwords.
Usually these password managers will have browser extensions that will automatically detect that you have a password for the site your visiting, and offer to fill it in for you. They can also generate passwords for new sites when you are prompted to enter one.
Password managers really are very good, very user friendly and very secure. 
Sometimes they're very graphical and user friendly, like 1password here, 

+++

### Passwords

![password-store](assets/pass.png)

Note:
And sometimes they're very geeky, like this one that I use.

---

## How do I collect it?

Two supported options for electronic surveys at ACU:

- Qualtrics |
- REDCap |

Note:
There are two options for collecting survey data at ACU, and I'll speak about the pros and cons of each.

Of course surveys are not the only means of collecting personal information but they're probably the most common.

+++

### Qualtrics

- Site-license; Free to use for ACU staff and students |
- Log in with your university credentials |
- Straightforward interface for designing and distributing surveys |
- Ability to collect anonymous responses and send reminders and thank yous |
- SaaS - Data stored in the US |
- Limited options to anonymise data |

Note:
Qualtrics is an online survey design and distribution system which is available to all ACU staff and students for free. 

It features an easy-to-use interface to designing a survey and distributing it to participants, and can maintain anonymity for responses. The way this works is that Qualtrics keeps an internal identifier linking each person in a contact list, with the responses. When you export data or look into the responses, you can't see who provided which response.

But remember that if you collect information like postcode, gender and language spoken at home, then your data itself may be reidentifiable.

However, Qualtrics does not have the same level of controls over sensitive data as REDCap, and may not be the best choice if you are collecting personal information.

Firstly, Qualtrics is a software-as-a-service offering, meaning you access it using a web-browser and the application and all data is held on Qualtrics servers, which are currently located in US data centres, which is problematic for ARC funded projects, although it is something ACU are looking to fix.

Secondly, Qualtrics provides only limited options to anonymise data, leading to the possibility of unintended privacy breaches if you're not careful.

If you're collecting survey data and need to worry about protecting personal information, and the privacy of your subjects, for example if you're collecting any medical information, or any kind of sensitive information, then you should probably consider using a purpose built tool like REDCap.

+++
![qualtrics1](assets/qualtrics1.png)

Note:
To show you the sort of controls over anonymity Qualtrics provides, this is really it, and it doesn't exactly explain what happens. But if you look into the documentation, it reads:

+++

"When responses are gathered with the Anonymous Link, enabling this setting will remove the respondents’ **IP address and location data** from your results. When responses are gathered with the Individual Link, enabling this setting will remove the IP address and location data and disconnect the response from the contact who provided it. In this way, you can know which contacts have responded (through your distribution history and contact history), but not which response belongs to which contact."

Source: [Qualtrics](https://www.qualtrics.com/support/survey-platform/survey-module/survey-options/survey-termination/)

Note:
This actually reveals that Qualtrics use of an internal identifier to connect respondent with response is ordinarily accessible to the researcher, unless they check this anonymize option, which Qualtrics do not recommend.

I don't mean to speak ill of Qualtrics; it really is a fantastic tool, but you should be aware of these limitations and the other options out there when you are dealing with sensitive research data. 

Speaking of other options:

+++

### REDCap

- Open source application maintained by Vanderbilt University |
- Built expressly for managing clinical trials |
- Very strict terms and conditions for licensees |
- HIPAA compliant |
- Self-hosted - Data stored on ACU data centres |
- Steep learning curve |
- Difficult interface |

Note:
REDCap is an open source application built and maintained by Vanderbilt University for running clinical trials, but it's also capable of running simple surveys or data collection forms, i.e., where data is entered by the project personnel rather than by the participant themselves. This is pretty standard in clinical trials where a clinician would enter both identifying information for a subject plus relevant medical results into a secure portal.

Vanderbilt keep a very tight control over who can access the software through a license. I won't go into the details, but the purpose is to ensure that REDCap maintains HIPAA compliance. 

HIPAA is US legislation that provides data privacy and security provisions for safeguarding medical information.

REDCap is not SaaS, it is self-hosted, as required by the license. This means the application and all its data resides on premise in ACU's data centres, which is a big positive when it comes to complying with privacy legislation and ARC funding rules.

Unfortunately though, REDCap has a steep learning curve, and its interface is not as intuitive as Qualtrics.

+++

![REDCap1](assets/REDCap1.png)

Note:
Here are a couple of examples of the controls that REDCap gives you to maintain privacy.

For one, when you configure a survey field, you have the option of marking it as an identifying field, which will have the effect of removing it from your data exports. 

+++

![REDCap2](assets/REDCap2.png)

Note:
Furthermore, you can give your collaborators specific access to the response data that excludes these identifying fields,

+++

![REDCap3](assets/REDCap3.png)

Note:
There are other controls, like shifting all the dates and times of the responses by a random number so that sleuths can't identify people based on contextual information, such as comparing when a response was submitted and when someone was online.

There are other benefits of using REDCap, more than I can cover here.

If you're interested in using REDCap, get in touch with me and I can create an account for you.

REDCap is pretty unintuitive though, so I also run REDCap training courses occasionally and am thinking about trying it out as a webinar sometime soon. Look out for an email announcement or get in touch if you want me to get onto this more quickly.

---

![privacy](http://amberhawk.typepad.com/.a/6a0115709c6f9d970b01538e472c37970b-800wi)

Note:
I don't particularly find this cartoon funny, but remember, this topic is bereft of interesting images.

---

## Informed Consent

- Information Letter to Participants |
- Consent Form |
- Specifying: |
  - how anonymity will be maintained |
  - how confidentiality will be maintained |

Note:
The last thing I'm going to talk about is informed consent.

Of course you'll be aware that you need to seek ethics approval for any project that deals with human data or information, and as part of that, you need to present your participants with an information letter, and they must also sign a consent form. Together these constitute informed consent.

This information letter must state:
- if the study is anonymous, how anonymity will be maintained throughout the study;
- if the study is confidential, how confidentiality will be maintained throughout the study and the level of confidentiality;

Research Services is currently working on additional guidance material and policies in this space, so I won't say much more here but I will point to a couple of relevant guides at the end of the presentation.

---

## Summary

- Even if data doesn't contain names and addresses, it could still be identifiable |
- Use Network shares or CloudStor instead of Dropbox, OneDrive or SharePoint |
- Sharing is possible, if you have consent, and are careful |

Note:
So, to summarise:

- Remember that data can easily be identifiable even if you don't have names and addresses, and you need to protect it.
- Protect it first and foremost by storing it on a service that's built for doing that. Use a network share or CloudStor for this, indeed, use a network share or cloudstor for any research data.
- You may share your data with collaborators, provided you have consent from your participants to do so, but you should only share sanitised data that has had identifying information removed.
- This, you can do fairly simply, by keeping it in a separate file which has encryption or at the very least, password protection. 
- If you're running surveys to collect data, look into using REDCap. It has a steep learning curve and a clunky interface, but it is far better at protecting sensitive data than Qualtrics, which focuses on ease of use, but sacrifices some control over privacy.
- Finally, you need to think about this stuff in preparation for your ethics application, and not right at the end of your project.

---

## Summary

- Separate identifying information and keep it safe, preferably encrypted |
- Use REDCap instead of Qualtrics for surveys collecting medical information |
- Your ethics approval will require you to inform your participants how you will keep their personal information safe |

Note:
So, to summarise:

- Remember that data can easily be identifiable even if you don't have names and addresses, and you need to protect it.
- Protect it first and foremost by storing it on a service that's built for doing that. Use a network share or CloudStor for this, indeed, use a network share or cloudstor for any research data.
- You may share your data with collaborators, provided you have consent from your participants to do so, but you should only share sanitised data that has had identifying information removed.
- This, you can do fairly simply, by keeping it in a separate file which has encryption or at the very least, password protection. 
- If you're running surveys to collect data, look into using REDCap. It has a steep learning curve and a clunky interface, but it is far better at protecting sensitive data than Qualtrics, which focuses on ease of use, but sacrifices some control over privacy.
- Finally, you need to think about this stuff in preparation for your ethics application, and not right at the end of your project.

---

### Additional Reading

- [NHMRC's page on Ethical issues and resources](https://nhmrc.gov.au/research-policy/ethics/ethical-issues-and-resources)
- [ACU’s Guidelines for Applicants to the HREC](http://research.acu.edu.au/wp-content/uploads/2015/02/Guidelines_for_Applicants_to_the_HREC_201003.pdf)
- [ANDS guide to Sensitive Data](http://www.ands.org.au/working-with-data/sensitive-data)
- [Guide to encrypting files on Windows](https://www.intowindows.com/how-to-create-zip-file-with-password-in-windows-78/)
- [Keka, a Mac utility for archiving and encrypting files](https://www.keka.io/en/)
- This presentation: [inter.fyi/SensData/ILSTE](https://inter.fyi/SensData/ILSTE)

---

## If time permits...

+++

### Securely collect email address

Example:

How to collect email address from participants for an optional prize draw or follow-up?

Note:
You want to allow study participants to sign up to a voluntary follow-up after the study, or enter into a draw for a small token of appreciation for taking the survey. You don't need to know which were their responses, but you need them to give an email address to you.

How can you do this while protecting their anonymity? You've spent all this effort on data storage options, choosing the right tool, and stand to lose all that by including a 'please enter your email address here' question at the end.

+++

### Securely collect email address

Solution:

- Create a new survey with one field: Email
- Include comment that response data will not be linked
- Use survey termination to redirect participants to second survey

Note:
The solution is to create a second survey specifically for collecting participant email address (or whichever field will be used). 

Include explanatory information, like that this is voluntary, and that their email and responses cannot be linked (remember date shifting).

Both Qualtrics and REDCap have an option to redirect participants to a specific URL upon completion of the survey. Redirect participants upon completion of the actual survey, to the second survey.

You'll then have two distinct datasets: One for your actual survey responses, and one just for a list of email addresses for people who have entered the draw, or are interested in the follow up. These two datasets are distinct, and won't be able to be used to join the dots.

